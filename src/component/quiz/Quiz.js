import React, { Component } from "react";
// import axios from 'axios';
import './Quiz.scss';
import QuizData from '../../api/quiz.json';

class Quiz extends Component {


  componentDidMount(){
    this.getQuiz();
  }

  getQuiz(){
    fetch('http://localhost:5000/quiz')
    .then (response => response.json())
    .then (data =>{
      console.log(data);
      this.setState({
        quiz: data,
        isLoading: false,
      })
    })
    .catch(error => this.setState({ error, isLoading: false}));
  }

  // getQuiz(){
  //   axios.get('http://localhost:5000/quiz')
  //   .then(data => {
  //     console.log(data);
  //   });
  // }

  

  render() {
    return (

      <div>
        {QuizData.map((quizDetails, index) =>{
          return <div className="quizBox">
          <h2 className="queText">{quizDetails.question}</h2>
         <div className="ansBox">
            <div className="ansText">
            <ul>
              <li>{quizDetails.option}</li>
            </ul>
            
            </div>
            </div>
            </div>
          // return <div className="quizBox">
          //  <h2 className="queText">{quizDetails.question}</h2>
          // <div className="ansBox">
          //    <div className="ansText">
          //    <ul>
          //      <li>{quizDetails.option}</li>
          //    </ul>
             
          //    </div>
          //    </div>
          //    </div>
        })}
      </div>
    );
  }
}
export default Quiz;

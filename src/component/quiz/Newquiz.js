import React, { Component } from "react";
import "./Quiz.scss";
import axios from 'axios';

class Newquiz extends Component {
  componentWillMount() {
    this.getQuiz();
  }

  //   getQuiz() {
  //     fetch("https://quiz0120.herokuapp.com/quiz")
  //       .then(response => response.json())
  //       .then(data => {
  //         console.log(data);
  //         this.setState({
  //           quiz: response.data
  //         });
  //       })
  //       .catch(error => this.setState({ error }));
  //   }
  getQuiz() {
    axios.get("https://quiz0120.herokuapp.com/quiz").then(Response => {
      console.log(Response);
      this.setState({
          quiz: Response.data
      });
    });
  }

  render() {
    
    return (
      <div className="quizBox">
        {this.state.quiz.map(quiz => <h2>{quiz.question}</h2>)}
        <div className="ansBox">
          <div className="ansText">
            <ul>
              <li>o</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
export default Newquiz;

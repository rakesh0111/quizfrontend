import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Header.scss';
import { Navbar, NavItem, Nav } from 'react-bootstrap';

 class Header extends Component {
    render() {
        return (
            <div>
                <Navbar className="navBar" inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand className="wt">
                            <Link to="/">Quiz App's</Link>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav pullRight >
                        <NavItem className="wt">
                            <Link to="/quiz">Quiz</Link>
                        </NavItem>
                    </Nav>
                </Navbar>
            </div>
        )
    }
}

export default Header;
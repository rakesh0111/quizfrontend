import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './component/header/Header';
import './App.scss';
import Newquiz from './component/quiz/Newquiz';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
        <Header/>
        <Route exact path="/" component={Newquiz}/>
        </div>
      </Router>
    );
  }
}

export default App;
